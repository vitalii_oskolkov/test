# How to package jar file:
Execute command `mvn clean compile assembly:single` from the root package.

# How to run jar file
`java -cp <path_to_jar/finder.jar> com.ovi.lookup.Finder <path to origin file> <path to other file> <source element id (optional)>`

