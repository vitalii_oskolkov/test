package com.ovi.lookup.service;

import static java.lang.String.format;
import static java.util.Comparator.comparingDouble;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

import com.ovi.lookup.exception.FileReadException;
import com.ovi.lookup.exception.HtmlElementNotFoundException;
import com.ovi.lookup.model.RatingElement;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

public class LookupService {

    private RatingCalculationService ratingCalculationService = new RatingCalculationService();

    public RatingElement findSimilarElement(String originElementId, String originFilePath, String targetFilePath) {
        String originHtmlContent = readFile(originFilePath);
        String otherHtmlContent = readFile(targetFilePath);

        Document originDocument = Jsoup.parse(originHtmlContent);
        Elements originElements = originDocument.select("#" + originElementId);
        if (originElements.size() == 0) {
            throw new HtmlElementNotFoundException(
                    format("Element with id: '%s' was not found in origin HTML", originElementId));
        }
        Element originModel = originElements.get(0);

        return findSimilarElement(Jsoup.parse(otherHtmlContent), originModel);
    }

    private RatingElement findSimilarElement(Document targetDocument, Element originElement) {
        String baseSearchCriteria = "*";
        if (originElement.classNames().contains("btn")) {
            baseSearchCriteria = ".btn";
        }
        Elements similarElements = targetDocument.select(baseSearchCriteria);
        if (similarElements.size() == 0) {
            throw new HtmlElementNotFoundException("There are no suitable target element found.");
        }
        if (similarElements.size() == 1) {
            return ratingCalculationService.computeRatingElement(originElement, similarElements.get(0));
        }
        List<RatingElement> sortedByRating = similarElements.stream()
                .map(similarElement -> ratingCalculationService.computeRatingElement(originElement, similarElement))
                .sorted(comparingDouble(RatingElement::getSimilarityRating).reversed())
                .collect(toList());

        return sortedByRating.get(0);
    }

    public String buildPathToElement(Element element) {
        Elements parents = element.parents();
        Collections.reverse(parents);
        return parents.stream()
                .map(org.jsoup.nodes.Element::tagName)
                .collect(joining(" > "));
    }

    private String readFile(String filePath) {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            throw new FileReadException(format("Unable to read file by file path: '%s'", filePath), e);
        }
        return contentBuilder.toString();
    }
}
