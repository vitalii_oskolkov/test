package com.ovi.lookup.service;

import com.ovi.lookup.model.RatingElement;
import org.jsoup.nodes.Element;

import java.util.Objects;

public class RatingCalculationService {

    public RatingElement computeRatingElement(Element sourceElem, Element targetElem) {
        RatingElement ratingElement = new RatingElement(targetElem);
        // by tag
        if (Objects.equals(sourceElem.tagName(), targetElem.tagName())) {
            ratingElement.addRatingOne();
        } else {
            ratingElement.addNonCompatible();
        }
        // by class
        if (Objects.equals(sourceElem.classNames(), targetElem.classNames())) {
            ratingElement.addRatingOne();
        } else {
            int count = (int) sourceElem.classNames().stream()
                    .filter(sourceClass -> targetElem.classNames().contains(sourceClass))
                    .count();
            ratingElement.addRatingPartOf(count, sourceElem.classNames().size());
        }
        // by content
        String targetHtml = targetElem.html() == null ? "" : targetElem.html();
        if (Objects.equals(sourceElem.html(), targetHtml)) {
            ratingElement.addRatingOne();
        } else if (targetHtml.toUpperCase().contains(sourceElem.html().toUpperCase())) {
            ratingElement.addRatingHalf();
        } else {
            ratingElement.addNonCompatible();
        }
        // by title
        String sourceTitle = sourceElem.attr("title");
        String targetTitle = targetElem.attr("title") == null ? "" : targetElem.attr("title");
        if (Objects.equals(sourceTitle, targetTitle)) {
            ratingElement.addRatingOne();
        } else if (targetTitle.toUpperCase().contains(sourceTitle.toUpperCase())) {
            ratingElement.addRatingHalf();
        } else {
            ratingElement.addNonCompatible();
        }

        return ratingElement;
    }
}
