package com.ovi.lookup.exception;

public class FileReadException extends RuntimeException {

    public FileReadException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
