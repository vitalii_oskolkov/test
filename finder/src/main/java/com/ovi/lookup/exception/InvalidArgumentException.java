package com.ovi.lookup.exception;

public class InvalidArgumentException extends RuntimeException {

    public InvalidArgumentException(String msg) {
        super(msg);
    }
}
