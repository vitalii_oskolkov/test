package com.ovi.lookup.exception;

public class HtmlElementNotFoundException extends RuntimeException {

    public HtmlElementNotFoundException(String msg) {
        super(msg);
    }
}
