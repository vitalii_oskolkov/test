package com.ovi.lookup;

import static java.lang.String.format;
import static java.util.stream.Collectors.toList;

import com.ovi.lookup.exception.InvalidArgumentException;
import com.ovi.lookup.model.RatingElement;
import com.ovi.lookup.service.LookupService;

import java.util.Arrays;
import java.util.List;

public class Finder {

    private static final String DEFAULT_ORIGINAL_ELEMENT_ID = "make-everything-ok-button";

    public static void main(String[] args) {
        List<String> arguments = Arrays.stream(args).collect(toList());
        String originElementId = DEFAULT_ORIGINAL_ELEMENT_ID;
        if (arguments.size() > 2) {
            originElementId = arguments.get(2);
        }
        if (arguments.size() < 2) {
            throw new InvalidArgumentException("At least origin and sample file paths must be specified!");
        }
        String originFilePath = arguments.get(0);
        String otherFilePath = arguments.get(1);

        LookupService lookupService = new LookupService();
        RatingElement ratingElement = lookupService.findSimilarElement(originElementId, originFilePath, otherFilePath);
        System.out.println(
                format("Found element path: '%s'", lookupService.buildPathToElement(ratingElement.getElement())));
        System.out.println(format("Found element similarity: ~%s%%", ratingElement.getSimilarityPercent()));
        System.out.println(format("Found element content: %s", ratingElement.getElement()));
    }

}
