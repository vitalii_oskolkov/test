package com.ovi.lookup.model;

import org.jsoup.nodes.Element;

public class RatingElement {

    private Element element;
    private double similarityRating;
    private int operationCount;

    public RatingElement(Element element) {
        this.element = element;
    }

    public void addRatingPartOf(int part, int of) {
        this.operationCount++;
        if (of > 0) {
            this.similarityRating += (double) part / of;
        }
    }

    public void addRatingOne() {
        this.operationCount++;
        this.similarityRating++;
    }

    public void addRatingHalf() {
        addRatingPartOf(1, 2);
    }

    public void addNonCompatible() {
        this.operationCount++;
    }

    public Element getElement() {
        return element;
    }

    public double getSimilarityRating() {
        return similarityRating;
    }

    public int getSimilarityPercent() {
        return (int) (this.similarityRating / this.operationCount * 100);
    }

    @Override
    public String toString() {
        return "RatingElement{" +
                "element=" + element +
                ", similarityPercent=" + this.getSimilarityPercent() +
                '}';
    }
}
